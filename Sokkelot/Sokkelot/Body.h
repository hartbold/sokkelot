#pragma once

#include <SFML\Graphics.hpp>

#include "Entity.h"
#include "ResourceManager.hpp"

class Body : public Entity
{
	public:
	
		enum Type
		{
			Player,
		};
	
	public: 
		explicit Body(Type type, const TextureManager& textures);
		virtual void  drawCurrent(sf::RenderTarget& target,
								  sf::RenderStates states) const;

	private:

		Type mType;
		sf::Sprite mSprite;
};