#pragma once

#include <memory>
#include <vector>
#include <algorithm>

#include <assert.h>

#include <SFML\Graphics.hpp>


class SceneNode : public sf::Transformable, public sf::Drawable, sf::NonCopyable
{
	public:
		typedef std::unique_ptr<SceneNode> Ptr;

	public:
		SceneNode();

		void attachChild(Ptr child);
		Ptr detachChild(const SceneNode& node);

		void update(sf::Time dt);

	private:
		std::vector<Ptr> children;
		SceneNode* parent;

	private:

		sf::Transform getWorldTransform() const;
		sf::Vector2f getWorldPosition() const; 

		virtual void updateCurrent(sf::Time dt);
		void updateChildren(sf::Time dt);

		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const final;
		virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
		virtual void drawChildren(sf::RenderTarget& target, sf::RenderStates states) const;

};