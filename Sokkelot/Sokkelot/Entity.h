#pragma once

#include <SFML\Graphics.hpp>

#include "SceneNode.h"

class Entity : public SceneNode
{
	public:
		void setVelocity(sf::Vector2f vel);
		void setVelocity(float x, float y);

		sf::Vector2f getVelocity() const;

	private:
		sf::Vector2f mVelocity;

	private :
		virtual void updateCurrent(sf::Time dt);
};