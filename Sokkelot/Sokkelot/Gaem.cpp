#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>

#include "ResourceManager.hpp"


const sf::Time TimePerFrame = sf::seconds(1.0f / 60.0f);

class Gaem
{
	public:
		Gaem();
		void run();
		void processEvents();
		void update(sf::Time time);
		void handleInput(sf::Keyboard::Key key, bool leDa);

	private:
		sf::RenderWindow window;
		sf::CircleShape player;		
		bool movingUp;
		bool movingDown;
		bool movingLeft;
		bool movingRight;
};


int main()
{

	try
	{
		Gaem gaem;
		gaem.run();
	}
	catch (std::exception& e)
	{
		std::cout << "\n EXCEPTION: " << e.what() << std::endl;
	}
}

Gaem::Gaem()
: window(sf::VideoMode(800, 640), "Sokkelot")
, player(){ 
	player.setRadius(40.f);
	player.setPosition(100.f, 100.f);
	player.setFillColor(sf::Color::White);
}


void Gaem::run()
{

	sf::Clock deltaTimer;

	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	sf::Time deltaTime;

	sf::Font font;
	
	if (!font.loadFromFile("resources/fonts/diablo.ttf"))
	{
		std::cout << "Font not found!" << std::endl;
	}

	sf::Text fpsText;
	fpsText.setFont(font);

	sf::Text usText;
	usText.setFont(font);

	std::string fpsTextString;
	std::string usTextString;

	float fps = TimePerFrame.asSeconds();
	float us = 0.f;

	float controlTime = 0;

	//FPS PREVIEW
	fpsText.setFillColor(sf::Color::White);
	fpsText.setPosition(50.0f, 50.0f);
	fpsText.setCharacterSize(15);

	//TIME PER UPDATE
	usText.setFillColor(sf::Color::White);
	usText.setPosition(50.0f, 65.0f);
	usText.setCharacterSize(15);

	while (window.isOpen())
	{

		deltaTime = deltaTimer.restart();
		
		//control da fps (warramente, ya se mejorar�)
		controlTime += deltaTime.asSeconds();
		if (controlTime > 1.0f)
		{
			fps = 1 / deltaTime.asSeconds();
			us = (float) deltaTime.asMicroseconds();

			controlTime = 0;
		}
		

		fpsText.setString("fps: " + std::to_string((int) fps));
		usText.setString("us update: " + std::to_string((int) us));

		timeSinceLastUpdate += deltaTime;
		processEvents();

		ResourceManager<sf::Texture, Textures::ID>*  tex = new ResourceManager<sf::Texture, Textures::ID>();
		
		//ESTA LINEA PETA
		//tex->load(Textures::Player, "ASDASD");

		while (timeSinceLastUpdate > TimePerFrame)
		{
			
			//us = timeSinceLastUpdate.asSeconds();
			processEvents();
			timeSinceLastUpdate -= TimePerFrame;
			
			Gaem::update(TimePerFrame);
		}

		window.clear(sf::Color::Black);
		window.draw(fpsText);
		window.draw(usText);
		window.draw(player);
		window.display();

	}
}

void Gaem::processEvents()
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		switch (event.type){
			case sf::Event::KeyPressed:
				handleInput(event.key.code, true);
				break;
			case sf::Event::KeyReleased:
				handleInput(event.key.code, false);
				break;
			case sf::Event::Closed:
				window.close();
				break;
		}
			
	}
}

void Gaem::update(sf::Time timePerFrame)
{
	sf::Vector2f movement(0.f, 0.f);
	
	if (movingDown){
		movement.y += 1.f;
	}

	if (movingUp){
		movement.y -= 1.f;
	}

	if (movingLeft){
		movement.x -= 1.f;
	}

	if (movingRight){
		movement.x += 1.f;
	}

	//mrua ## ESTO PETA
	//player.move(movement * timePerFrame.asSeconds);

	//mru
	player.move(movement);
}

void Gaem::handleInput(sf::Keyboard::Key key, bool leDa){
	switch (key){
		case sf::Keyboard::W:
			movingUp = leDa;
			break;
		case sf::Keyboard::A:
			movingLeft = leDa;
			break;
		case sf::Keyboard::S:
			movingDown = leDa;
			break;
		case sf::Keyboard::D:
			movingRight = leDa;
			break;
	}
}

