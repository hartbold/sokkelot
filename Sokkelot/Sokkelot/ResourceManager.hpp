#pragma once
#include <memory>

#include <stdio.h>
#include <assert.h>

#include "SFML\Graphics.hpp"

namespace Textures
{
	enum ID { Player };
}

template <typename Resource, typename Identifier>
class ResourceManager
{
	public:

		void load(Identifier id, const std::string& filename);

		template <typename Parameter>
		void load(Identifier id, const std::string& filename, const Parameter& secondParam);

		Resource& get(Identifier id);
		const Resource& get(Identifier id) const;

	private:
		std::map<Identifier, std::unique_ptr<Resource>> textureMap;
};

typedef ResourceManager<sf::Texture, Textures::ID> TextureManager;

#include "ResourceManager.inl"