#pragma once

#include <array>

#include <SFML\Graphics.hpp>

#include "ResourceManager.hpp"
#include "SceneNode.h"


class World : private sf::NonCopyable
{
	public:
		explicit World(sf::RenderWindow & window);
	
		void update();
		void draw();
	
	private:
		void loadTextures();
		void buildScene();
	
	private:
		enum Layer
		{
			Background,
			Object,
			LayerCount,
		};

	private:
		sf::RenderWindow mWindow;
		sf::View mWorldView;
		TextureManager mTextures;
		SceneNode mSceneGraph;
		std::array<SceneNode*, LayerCount> mSceneLayers;

		sf::FloatRect mWorldBounds;
		sf::Vector2f mSpawnPosition;
		
		//float  mScrollSpeed;
		//player
};