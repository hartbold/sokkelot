#pragma once

#include "Body.h"

Textures::ID toTextureID(Body::Type type)
{
	switch (type)
	{
		case Body::Player:
			return Textures::Player;
	}
}

Body::Body(Type type, const TextureManager& textures) :
	mType(type), mSprite(textures.get(toTextureID(type))) 
{
	sf::FloatRect bounds = mSprite.getLocalBounds();
	mSprite.setOrigin(bounds.width / 2.f, bounds.height / 2.f);
}

void Body::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(mSprite, states);
}